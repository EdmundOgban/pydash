#include <stdio.h>
#include <stdlib.h>
#include <stdint.h>
#include <string.h>
#include <math.h>

#include <samplerate.h>


static inline float clamp(float v, float min, float max) {
    return (v > max ? max : (v < min ? min : v));
}

static inline int16_t float_to_int16(float v) {
    v = clamp(v, -1, 1);

    return (int16_t)((v < 0) ? v * 32768 : v * 32767);
}

static inline float int16_to_float(int16_t v) {
    return (v < 0) ? v / (float)32768 : v / (float)32767;
}

static inline int8_t float_to_int8(float v) {
    v = clamp(v, -1, 1);

    return (int8_t)((v < 0) ? v * 256 : v * 255);
}

static inline float int8_to_float(int8_t v) {
    return (v < 0) ? v / (float)256 : v / (float)255;
}

// TODO
// #define NORMALIZE_TEMPLATE(in_buf) do {       \
// } while(0);
// 
// static inline void normalize_int8(in_buf, in_buf_len, in_buf_normalized) {
// }

uint64_t samplerate_resample(uint8_t * in_buf,
                             uint64_t in_buf_samples,
                             uint8_t ** out_buf,
                             uint32_t src_srate,
                             uint32_t dst_srate,
                             uint8_t channels,
                             uint8_t depth) {
    SRC_STATE * src_ctx;
    SRC_DATA src_data;
    int src_err;

    int16_t * in_buf_i16 = (int16_t *)in_buf;
    float * in_buf_normalized;
    int16_t * out_buf_i16;
    float * out_buf_resampled;
    uint64_t out_buf_size;

    double src_ratio = (double)dst_srate / src_srate;


    src_ctx = src_new(SRC_SINC_MEDIUM_QUALITY, channels, &src_err);
    memset(&src_data, 0, sizeof(SRC_DATA));
    
    in_buf_normalized = malloc(in_buf_samples * channels * sizeof(float));
    out_buf_size = (uint64_t)ceil(in_buf_samples * sizeof(float) * src_ratio);
    out_buf_resampled = malloc(out_buf_size  * channels);

    for (uint64_t i = 0; i < in_buf_samples; i++) {
        in_buf_normalized[i] = int16_to_float(in_buf_i16[i]);
    }

    src_data.data_in = in_buf_normalized;
    src_data.input_frames = in_buf_samples / channels;
    src_data.data_out = out_buf_resampled;
    src_data.output_frames = out_buf_size;
    src_data.src_ratio = src_ratio;
    src_data.end_of_input = 1;

    src_process(src_ctx, &src_data);

    if (src_data.output_frames_gen == 0) {
        puts("    ---- Resampling produced no frames!");
        *out_buf = NULL;
    }
    else {
        out_buf_i16 = malloc(src_data.output_frames_gen * channels * sizeof(int16_t));

        for (uint64_t i = 0; i < src_data.output_frames_gen * channels; i++) {
            out_buf_i16[i] = float_to_int16(out_buf_resampled[i]);
        }

        *out_buf = (uint8_t *)out_buf_i16;
    }

    free(in_buf_normalized);
    free(out_buf_resampled);
    src_delete(src_ctx);

    return src_data.output_frames_gen * channels;
}