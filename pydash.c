#include <stdio.h>
#include <stdlib.h>
#include <stdint.h>
#include <stdbool.h>

#include <Python.h>
#include <vlc_common.h>
#include <vlc_stream.h>
#include <neaacdec.h>

#include "libmp4.h"
#include "aac_header.h"
#include "richbuffer.h"
#include "pcm.h"
#include "resample.h"

#define CHUNK_SIZE 8192
#define TARGET_SAMPLERATE 48000

#define PY_RAISE_OOM() PyErr_SetString(PyExc_MemoryError, "Out of Memory.")


uint8_t * vlc_buf;
stream_t * s;


int init(uint8_t ** vlc_buf, stream_t ** s) {
    size_t len;

    *vlc_buf = vlc_alloc(len, 1);
    if (*vlc_buf == NULL)
        return 0;

    *s = vlc_stream_MemoryNew(NULL, *vlc_buf, len, false);
    if (s == NULL) {
        free(*vlc_buf)
        return 0;
    }

    return 1;
}

int faad_decode_next(struct richbuffer * aac_buf, uint8_t ** decoded_buf,
                     NeAACDecFrameInfo *frm_info) {
    unsigned long samplerate;
    uint8_t channels;
    int8_t err;
    static NeAACDecHandle faad_ctx;
    int ret = 0;

    if (faad_ctx == NULL) {
        faad_ctx = NeAACDecOpen();
        err = NeAACDecInit(
            faad_ctx, aac_buf->scrub, aac_buf->len, &samplerate, &channels);

        if (err < 0) {
            msg_Dbg(stderr, "NeAACDecInit error: %d\n", err);
            return -1;
        }

        aac_buf->scrub += err;
        aac_buf->len -= err;
        msg_vvDbg(stderr, "{ samplerate: %lu, channels: %u, bytesRead: %d }\n",
            samplerate, channels, err);
    }

    *decoded_buf = NeAACDecDecode(faad_ctx, frm_info, aac_buf->scrub, aac_buf->len);

    if ((frm_info->error == 0) && (frm_info->samples > 0)) {
        aac_buf->scrub += frm_info->bytesconsumed;
        aac_buf->len -= frm_info->bytesconsumed;
        ret = 1;
    }
    else if (frm_info->error != 0) {
        msg_Dbg(stderr, "NeAACDecode error: %d\n", frm_info->error);
        msg_Dbg(stderr, "%s\n", NeAACDecGetErrorMessage(frm_info->error));
    }
    else {
      msg_Dbg(stderr, "got nothing...\n");
    }

    return ret;
}

static PyObject * module_feed_chunk(PyObject * self, PyObject * args) {
    MP4_Box_t * root_box;

    if (s == NULL) {
        if (!init(&vlc_buf, &f, &fw, &s)) {
            PY_RAISE_OOM();
            return NULL;
        }
            
        root_box = MP4_BoxGetRoot(s);

        if (root_box == NULL) {
            msg_Dbg(NULL, "Cannot get root box.");
            return NULL;
        }
    }

}

static PyObject * module_next_packet(PyObject * self, PyObject * args) {
    struct richbuffer aac_buf;
    struct richbuffer mp4_buf;
    struct richbuffer pcm_converted;
    uint8_t * pcm_buf;

    MP4_Box_t * trun_box_chunk;
    MP4_Box_t * mdat_box_chunk;
    MP4_Box_t * trun_box_view;
    MP4_Box_t * mdat_box_view;
    MP4_Box_data_trun_t * p_trun;

    NeAACDecFrameInfo faad_frminfo;

    // TODO: Get these informations from MP4 headers
    uint32_t mp4_samplerate = 44100;
    uint8_t mp4_channels = 2;
    uint8_t mp4_depth = 16;


    if (s == NULL) {
        PyErr_SetString(PyExc_RuntimeError,
            "Function called without a valid context.");
        return NULL;
    }

    trun_box_chunk = MP4_BoxGetNextChunk(s);
    if (trun_box_chunk == NULL) {
        // TODO: Raise exception
        goto exc_exit;
    }

    trun_box_view = MP4_BoxGet(trun_box_chunk, "moof/traf/trun");
    if (trun_box_view == NULL)
        Py_RETURN_NONE;

    mdat_box_chunk = MP4_BoxGetNextChunk(s);
    if (mdat_box_chunk == NULL) {
        // TODO: Raise exception
        goto exc_exit;
    }

    mdat_box_view = MP4_BoxGet(mdat_box_chunk, "mdat");
    if (mdat_box_view == NULL)
        Py_RETURN_NONE;

    p_trun = trun_box_view->data.p_trun;

    // FIXME: Trusting user data
    mp4_buf.size = mdat_box_view->i_size - 8;
    aac_buf.size = mp4_buf.size + 7 * p_trun->i_sample_count;

    richbuff_alloc(&mp4_buf);
    richbuff_alloc(&aac_buf);

    mp4_buf.len = mp4_buf.size;
    aac_buf.len = aac_buf.size;

    realing_pos = vlc_stream_Tell(s);
    vlc_stream_Seek(s, mdat_box_view->i_pos + 8);
    vlc_stream_Read(s, mp4_buf.buf, mp4_buf.size);

    if (s->starving) {
        Py_RETURN_FALSE;
    }

    for (uint32_t i = 0; i < p_trun->i_sample_count; i++) {
        MP4_descriptor_trun_sample_t * sample = &p_trun->p_samples[i];
        uint64_t aac_header = aac_compile_header(
            mp4_samplerate, mp4_channels, sample->i_size);

        memcpy(aac_buf.scrub, (char *)&aac_header, 7);
        aac_buf.scrub += 7;

        memcpy(aac_buf.scrub, mp4_buf.scrub, sample->i_size);

        aac_buf.scrub += sample->i_size;
        mp4_buf.scrub += sample->i_size;
    }

    richbuff_reset_scrub(&aac_buf);

    while (faad_decode_next(&aac_buf, &pcm_buf, &faad_frminfo) > 0) {
        uint8_t channels = faad_frminfo.channels;
        uint64_t samples = faad_frminfo.samples;
        bool converted = false;
        bool resampled = false;

        if (faad_frminfo.samplerate != TARGET_SAMPLERATE) {
            uint8_t * pcm_resampled_buf;
        
            samples = samplerate_resample(
                pcm_buf, samples, &pcm_resampled_buf,
                faad_frminfo.samplerate, TARGET_SAMPLERATE, channels, mp4_depth);

            pcm_buf = pcm_resampled_buf;
            resampled = true;
        }

        if (channels == 1) {
            int ret;
            struct richbuffer pcm_richbuf;

            if (pcm_converted.buf == NULL) {
                pcm_converted.size = samples * (mp4_depth / 8);
                if (!richbuff_alloc(&pcm_converted)) {
                    PY_RAISE_OOM();
                    goto exc_exit;
                }
            }

            richbuff_wrap(&pcm_richbuf, pcm_buf, samples);
            ret = pcm_convert_m_to_s(
                &pcm_richbuf, &pcm_richbuf, &pcm_converted, mp4_depth);
            channels = 2;

            free(pcm_buf);
            pcm_buf = pcm_converted.buf;
            converted = true;
        }

        fwrite(pcm_buf, samples * (mp4_depth / 8), 1, fw);

        if (resampled || converted)
            free(pcm_buf);
    }

    Msg_vvDbg(stderr, "    ---- mp4 trun/mdat box done\n");

    // richbuff_free(&mp4_buf);
    // richbuff_free(&aac_buf);

    // MP4_BoxFree(trun_box_chunk);
    // MP4_BoxFree(mdat_box_chunk);

    Py_RETURN_TRUE;

exc_exit:
    module_destroy(self, args);

    return NULL;
}

static PyObject * module_destroy(PyObject * self, PyObject * args) {

    if (root_box != NULL)
        MP4_BoxFree(root_box);

    if (s != NULL)
        vlc_stream_Delete(s);

    if (vlc_buf != NULL)
        free(vlc_buf);
}

// Method definition object for this extension, these argumens mean:
// ml_name: The name of the method
// ml_meth: Function pointer to the method implementation
// ml_flags: Flags indicating special features of this method, such as
//          accepting arguments, accepting keyword arguments, being a
//          class method, or being a static method of a class.
// ml_doc:  Contents of this method's docstring
static PyMethodDef module_methods[] = { 
    {   
        "destroy", module_destroy, METH_NOARGS,
        "Deallocates decoding engine."
    },
    {   
        "feed_chunk", module_feed_chunk, METH_VARARGS,
        "Feeds chunked MP4 data."
    },
    {   
        "next_packet", module_next_packet, METH_NOARGS,
        "Gets audio data per chunk."
    },
    {NULL, NULL, 0, NULL}
};


// Module definition
// The arguments of this structure tell Python what to call your extension,
// what it's methods are and where to look for it's method definitions
static struct PyModuleDef module_definition = { 
    PyModuleDef_HEAD_INIT,
    "pydash",
    "An MP4-DASH incapsulated AAC stream decoder.",
    -1,
    module_methods
};

// Module initialization
// Python calls this function when importing your extension. It is important
// that this function is named PyInit_[[your_module_name]] exactly, and matches
// the name keyword argument in setup.py's setup() call.
PyMODINIT_FUNC PyInit_pydash(void) {
    PyObject * module;
    Py_Initialize();
    
    module = PyModule_Create(&module_definition);
    // PyExc_OpusDecode = PyErr_NewException("pydash.OpusDecodeException", NULL, NULL);

    // PyModule_AddObject(module, "OpusDecodeException", PyExc_OpusDecode);

    return module;
}