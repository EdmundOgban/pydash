#include <stdio.h>
#include <stdlib.h>
#include <stdint.h>
#include <stdbool.h>

#include <vlc_common.h>
#include <vlc_stream.h>

ssize_t vlc_stream_Peek(stream_t * s, const uint8_t ** restrict bufp, size_t len) {
    size_t max_read_len = s->size - s->pos;
    void * tmpbuf;

    msg_vvDbg(stderr, "vlc_stream_Peek(len:%lu buf->pos:%lu)\n", len, s->pos);

    if (len > max_read_len) {
        len = max_read_len;
        s->starving = true;
    }
    else {
        s->starving = false;
    }

    if (s->peekbuf == NULL) {
        s->peekbuf = vlc_alloc(len, sizeof(s->buf_element_size));

        if (s->peekbuf == NULL) {
            return VLC_ENOMEM;
        }

        s->peekbuf_size = len;
    }
    else if (len > s->peekbuf_size) {
        tmpbuf = realloc(s->peekbuf, len);

        if (tmpbuf == NULL) {
            free(s->peekbuf);
            return VLC_ENOMEM;
        }

        s->peekbuf = tmpbuf;
    }

    memcpy(s->peekbuf, s->buf + s->pos, len);
    *bufp = s->peekbuf;

    return len;
}

int vlc_stream_Seek(stream_t * s, uint64_t offset) {
    msg_vvDbg(stderr, "vlc_stream_Seek(offset:%lu buf->pos:%lu)\n", offset, s->pos);

    s->last_pos = vlc_stream_Tell(s);
    s->pos = offset;

    return 0;
}

int vlc_stream_Unseek(stream_t * s) {
    msg_vvDbg(stderr, "vlc_stream_Unseek()");

    if (s->last_pos != 0) {
        s->pos = s->last_pos;
        s->last_pos = 0;
    }

    return 0;
}

// Not supporting buf == NULL
ssize_t vlc_stream_Read(stream_t * s, void * buf, size_t len) {
    size_t max_read_len = s->size - s->pos;

    if (len > max_read_len) {
        len = max_read_len;
        s->starving = true;
    }
    else {
        s->starving = false;
    }

    if (buf == NULL)
        return 0;

    msg_vvDbg(stderr, "vlc_stream_Read(len:%lu buf->pos:%lu)\n", len, s->pos);
    memcpy(buf, s->buf + s->pos, len);

    s->pos += len;

    return len;
}

uint64_t vlc_stream_Tell(const stream_t * s) {
    msg_vvDbg(stderr, "vlc_stream_Tell()\n");
    return s->pos;
}

int vlc_stream_GetSize(stream_t * s, uint64_t * size) {
    msg_vvDbg(stderr, "vlc_stream_GetSize()\n");
    *size = s->size;

    return VLC_SUCCESS;
}

stream_t * vlc_stream_MemoryNew(void * p_this, uint8_t * p_buffer,
                                size_t i_size, bool preserve) {

    stream_t * s;

    s = calloc(sizeof(stream_t), 1);

    if (s != NULL) {
        s->buf = vlc_alloc(i_size, 1);

        if (s->buf == NULL) {
            free(s);
            return NULL;
        }

        s->size = i_size;
        s->preserve = preserve;

        memcpy(s->buf, p_buffer, i_size);
    }

    msg_vvDbg(stderr, "vlc_stream_MemoryNew alloc_request:%lu stream_t(addr):%p"
        " buf(addr):%p buf->pos:%lu\n", i_size, s, s->buf, s->pos);

    return s;
}

void vlc_stream_Delete(stream_t * s) {
    msg_vvDbg(stderr, "vlc_stream_Delete()\n");

    // FIXME: Check for memory leaks
    if (!s->preserve)
        free(s->buf);

    free(s->peekbuf);
    free(s);
    // memset(s, 0, sizeof(stream_t));
}

int vlc_stream_Control(stream_t * s, enum stream_query_e action, bool * res) {
    msg_vvDbg(stderr, "vlc_stream_Control query:%u\n", action);

    switch (action) {
        case STREAM_CAN_SEEK:
            *res = true;
            return VLC_SUCCESS;
        break;

        default:
            return VLC_EGENERIC;
        break;
    }
}

