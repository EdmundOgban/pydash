#include <stdio.h>
#include <stdint.h>
#include <string.h>

#include <vlc_common.h>

#include "aac_header.h"


static inline int16_t aac_sample_frequency_idx(const uint64_t sample_freq) {
    switch (sample_freq) {
        case 96000: return  0; break;
        case 88200: return  1; break;
        case 64000: return  2; break;
        case 48000: return  3; break;
        case 44100: return  4; break;
        case 32000: return  5; break;
        case 24000: return  6; break;
        case 22050: return  7; break;
        case 16000: return  8; break;
        case 12000: return  9; break;
        case 11025: return 10; break;
        case  8000: return 11; break;
        case  7350: return 12; break;
        default:    return -1; break;
    }
}

uint64_t aac_compile_header(uint32_t sample_freq, uint8_t channels, uint16_t block_len) {
    aac_adts_header_t hdr;

    memset(&hdr, 0xFF, 7);

    hdr.fields.mpeg_version = AAC_ADTS_MPEG4;
    hdr.fields.layer = 0; // Always 0
    hdr.fields.protect_absent = 1; // Set to 1 if CRC is absent
    
    hdr.fields.profile = 1; // AAC Main
    hdr.fields.sample_freq = aac_sample_frequency_idx(sample_freq) & 0x0F;
    
    hdr.fields.private = 0; // Always 0
    
    hdr.fields.channel_config = channels;
    
    hdr.fields.original = 0;
    hdr.fields.home = 0;
    hdr.fields.copyright_id = 0;
    hdr.fields.copyright_start = 0;
    hdr.fields.frame_length = (7 + block_len) & 0x1FFF;
    
    hdr.fields.raw_data_blocks = 0; // Not sure about this

    return hton64(hdr.whole) >> 8;
}