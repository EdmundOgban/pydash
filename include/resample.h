#include <stdint.h>

#ifndef RESAMPLE_H_
#define RESAMPLE_H_

uint64_t samplerate_resample(uint8_t * in_buf,
                             uint64_t in_buf_len,
                             uint8_t ** out_buf,
                             uint32_t src_srate,
                             uint32_t dst_srate,
                             uint8_t channels,
                             uint8_t depth);

#endif /* RESAMPLE_H_ */