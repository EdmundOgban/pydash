#include <stdlib.h>
#include <stdint.h>
#include <stdbool.h>

#include <vlc_block.h>

#ifndef VLC_STREAM_H_
#define VLC_STREAM_H_

typedef struct stream_s {
    uint8_t buf_element_size;
    uint8_t * buf;
    uint64_t pos;
    uint64_t last_pos;
    size_t len;
    uint64_t size;
    uint8_t * peekbuf;
    uint8_t peekbuf_size;
    bool preserve;
    bool starving;
} stream_t;

enum stream_query_e
{
    /* capabilities */
    STREAM_CAN_SEEK,            /**< arg1= bool *   res=cannot fail*/
    STREAM_CAN_FASTSEEK,        /**< arg1= bool *   res=cannot fail*/
    STREAM_CAN_PAUSE,           /**< arg1= bool *   res=cannot fail*/
    STREAM_CAN_CONTROL_PACE,    /**< arg1= bool *   res=cannot fail*/
    /* */
    STREAM_GET_SIZE=6,          /**< arg1= uint64_t *     res=can fail */

    /* */
    STREAM_GET_PTS_DELAY = 0x101,/**< arg1= vlc_tick_t* res=cannot fail */
    STREAM_GET_TITLE_INFO, /**< arg1=input_title_t*** arg2=int* res=can fail */
    STREAM_GET_TITLE,       /**< arg1=unsigned * res=can fail */
    STREAM_GET_SEEKPOINT,   /**< arg1=unsigned * res=can fail */
    STREAM_GET_META,        /**< arg1= vlc_meta_t *       res=can fail */
    STREAM_GET_CONTENT_TYPE,    /**< arg1= char **         res=can fail */
    STREAM_GET_SIGNAL,      /**< arg1=double *pf_quality, arg2=double *pf_strength   res=can fail */
    STREAM_GET_TAGS,        /**< arg1=const block_t ** res=can fail */

    STREAM_SET_PAUSE_STATE = 0x200, /**< arg1= bool        res=can fail */
    STREAM_SET_TITLE,       /**< arg1= int          res=can fail */
    STREAM_SET_SEEKPOINT,   /**< arg1= int          res=can fail */

    /* XXX only data read through vlc_stream_Read/Block will be recorded */
    STREAM_SET_RECORD_STATE,     /**< arg1=bool, arg2=const char *psz_ext (if arg1 is true)  res=can fail */

    STREAM_SET_PRIVATE_ID_STATE = 0x1000, /* arg1= int i_private_data, bool b_selected    res=can fail */
    STREAM_SET_PRIVATE_ID_CA,             /* arg1= void * */
    STREAM_GET_PRIVATE_ID_STATE,          /* arg1=int i_private_data arg2=bool *          res=can fail */
};

// Sets stream position counter.
int vlc_stream_Seek(stream_t *, uint64_t);
// Restores stream position to last seeked/read position.
int vlc_stream_Unseek(stream_t * s);
// Reads from buffer without moving position counter.
ssize_t vlc_stream_Peek(stream_t * s, const uint8_t ** restrict, size_t);
// Reads from buffer, moves position counter.
ssize_t vlc_stream_Read(stream_t * s, void * buf, size_t len);
// Tells the current stream position
uint64_t vlc_stream_Tell(const stream_t *);
// Get the size of the stream. Returns [VLC_SUCCESS | VLC_EGENERIC]
int vlc_stream_GetSize(stream_t *, uint64_t * size);
// Create a stream from a memory buffer.
stream_t * vlc_stream_MemoryNew(void *, uint8_t *, size_t, bool);  
// Deletes a stream.
void vlc_stream_Delete(stream_t *);
// Controls a stream. (stripped down version, implements only CAN_SEEK)
int vlc_stream_Control(stream_t *, enum stream_query_e, bool *);

static inline int64_t stream_Size( stream_t *s ) {
    uint64_t i_pos;

    if (vlc_stream_GetSize(s, &i_pos))
        return 0;

    if(i_pos >> 62)
        return (int64_t)1 << 62;

    return i_pos;
}

#endif /* VLC_STREAM_H_ */
