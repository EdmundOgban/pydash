#include <stdio.h>
#include <stdlib.h>
#include <stdint.h>
#include <stdarg.h>
#include <stdbool.h>
#include <string.h>
#include <inttypes.h>


#ifndef VLC_COMMON_H_
#define VLC_COMMON_H_

/* Try to fix format strings for all versions of mingw and mingw64 */
#if defined(__x86_64__)
    #undef PRId64
    #define PRId64 "ld"
    #undef PRIi64
    #define PRIi64 "li"
    #undef PRIu64
    #define PRIu64 "lu"
    #undef PRIo64
    #define PRIo64 "lo"
    #undef PRIx64
    #define PRIx64 "lx"
#endif
#if defined(__i386__)
    #undef PRId64
    #define PRId64 "lld"
    #undef PRIi64
    #define PRIi64 "lli"
    #undef PRIu64
    #define PRIu64 "llu"
    #undef PRIo64
    #define PRIo64 "llo"
    #undef PRIx64
    #define PRIx64 "llx"
#endif


/*****************************************************************************
 * Error values (shouldn't be exposed)
 *****************************************************************************/
/** No error */
#define VLC_SUCCESS        (-0)
/** Unspecified error */
#define VLC_EGENERIC       (-1)
/** Not enough memory */
#define VLC_ENOMEM         (-2)
/** Timeout */
#define VLC_ETIMEOUT       (-3)
/** Module not found */
#define VLC_ENOMOD         (-4)
/** Object not found */
#define VLC_ENOOBJ         (-5)
/** Variable not found */
#define VLC_ENOVAR         (-6)
/** Bad variable value */
#define VLC_EBADVAR        (-7)
/** Item not found */
#define VLC_ENOITEM        (-8)


#define VLC_MSG_INFO "[Info]"
#define VLC_MSG_ERR "[ERR]"
#define VLC_MSG_WARN "[Warn]"
#define VLC_MSG_DEBUG "[Debug]"

#define msg_Generic(p_this, msg_level, fmt, ...) \
    fprintf(stderr, "%s " fmt, msg_level, ##__VA_ARGS__)

#define msg_Info(p_this, ...) \
    msg_Generic(p_this, VLC_MSG_INFO, __VA_ARGS__)
#define msg_Err(p_this, ...) \
    msg_Generic(p_this, VLC_MSG_ERR, __VA_ARGS__)
#define msg_Warn(p_this, ...) \
    msg_Generic(p_this, VLC_MSG_WARN, __VA_ARGS__)

#ifdef DEBUG
    #define msg_Dbg(p_this, ...) \
        msg_Generic(p_this, VLC_MSG_DEBUG, __VA_ARGS__)

    #ifdef VERY_VERBOSE_DEBUG
        #define msg_vvDbg(p_this, ...) \
            msg_Generic(p_this, VLC_MSG_DEBUG, __VA_ARGS__)
    #else
        #define msg_vvDbg(p_this, ...)
    #endif

#else
    #define msg_Dbg(p_this, ...)
    #define msg_vvDbg(p_this, ...)
#endif

/**
 * \defgroup cext C programming language extensions
 * \ingroup vlc
 *
 * This section defines a number of macros and inline functions extending the
 * C language. Most extensions are implemented by GCC and LLVM/Clang, and have
 * unoptimized fallbacks for other C11/C++11 conforming compilers.
 * @{
 */
#ifdef __GNUC__
# define VLC_GCC_VERSION(maj,min) \
    ((__GNUC__ > (maj)) || (__GNUC__ == (maj) && __GNUC_MINOR__ >= (min)))
#else
/** GCC version check */
# define VLC_GCC_VERSION(maj,min) (0)
#endif

/* Branch prediction */
#if defined (__GNUC__) || defined (__clang__)
    #define likely(p)     __builtin_expect(!!(p), 1)
    #define unlikely(p)   __builtin_expect(!!(p), 0)
    #define unreachable() __builtin_unreachable()
#else
    /**
     * Predicted true condition
     *
     * This macro indicates that the condition is expected most often true.
     * The compiler may optimize the code assuming that this condition is usually
     * met.
     */
    #define likely(p)     (!!(p))

    /**
     * Predicted false condition
     *
     * This macro indicates that the condition is expected most often false.
     * The compiler may optimize the code assuming that this condition is rarely
     * met.
     */
    #define unlikely(p)   (!!(p))

    /**
     * Impossible branch
     *
     * This macro indicates that the branch cannot be reached at run-time, and
     * represents undefined behaviour.
     * The compiler may optimize the code assuming that the call flow will never
     * logically reach the point where this macro is expanded.
     *
     * See also \ref vlc_assert_unreachable.
     */
    #define unreachable() ((void)0)
#endif


#if !(VLC_GCC_VERSION(5,0) || defined(__clang__))
# include <limits.h>
#endif

static inline bool umul_overflow(unsigned a, unsigned b, unsigned *res)
{
#if VLC_GCC_VERSION(5,0) || defined(__clang__)
     return __builtin_umul_overflow(a, b, res);
#else
     *res = a * b;
     return b > 0 && a > (UINT_MAX / b);
#endif
}

static inline bool umull_overflow(unsigned long a, unsigned long b,
                                  unsigned long *res)
{
#if VLC_GCC_VERSION(5,0) || defined(__clang__)
     return __builtin_umull_overflow(a, b, res);
#else
     *res = a * b;
     return b > 0 && a > (ULONG_MAX / b);
#endif
}

static inline bool umulll_overflow(unsigned long long a, unsigned long long b,
                                   unsigned long long *res)
{
#if VLC_GCC_VERSION(5,0) || defined(__clang__)
     return __builtin_umulll_overflow(a, b, res);
#else
     *res = a * b;
     return b > 0 && a > (ULLONG_MAX / b);
#endif
}

/**
 * Overflowing multiplication
 *
 * Converts \p a and \p b to the type of \p *r.
 * Then computes the product of both conversions while checking for overflow.
 * Finally stores the result in \p *r.
 *
 * \param a an integer
 * \param b an integer
 * \param r a pointer to an integer [OUT]
 * \retval false The product did not overflow.
 * \retval true The product overflowed.
 */
#define mul_overflow(a,b,r) \
    _Generic(*(r), \
        unsigned: umul_overflow(a, b, (unsigned *)(r)), \
        unsigned long: umull_overflow(a, b, (unsigned long *)(r)), \
        unsigned long long: umulll_overflow(a, b, (unsigned long long *)(r)))


#define VLC_FORMAT(a, b)
#define VLC_USED __attribute__((warn_unused_result))
#define VLC_UNUSED(x) (void)(x)
#define VLC_MALLOC __attribute__((malloc))

VLC_USED VLC_MALLOC
static inline void *vlc_alloc(size_t count, size_t size)
{
    return mul_overflow(count, size, &size) ? NULL : malloc(size);
}

#ifdef WORDS_BIGENDIAN
# define hton16(i) ((uint16_t)(i))
# define hton32(i) ((uint32_t)(i))
# define hton64(i) ((uint64_t)(i))
#else
# define hton16(i) vlc_bswap16(i)
# define hton32(i) vlc_bswap32(i)
# define hton64(i) vlc_bswap64(i)
#endif
#define ntoh16(i) hton16(i)
#define ntoh32(i) hton32(i)
#define ntoh64(i) hton64(i)

/** Byte swap (16 bits) */
VLC_USED
static inline uint16_t vlc_bswap16(uint16_t x)
{
    return (x << 8) | (x >> 8);
}

/** Byte swap (32 bits) */
VLC_USED
static inline uint32_t vlc_bswap32(uint32_t x)
{
#if defined (__GNUC__) || defined(__clang__)
    return __builtin_bswap32 (x);
#else
    return ((x & 0x000000FF) << 24)
         | ((x & 0x0000FF00) <<  8)
         | ((x & 0x00FF0000) >>  8)
         | ((x & 0xFF000000) >> 24);
#endif
}

/** Byte swap (64 bits) */
VLC_USED
static inline uint64_t vlc_bswap64(uint64_t x)
{
#if defined (__GNUC__) || defined(__clang__)
    return __builtin_bswap64 (x);
#elif !defined (__cplusplus)
    return ((x & 0x00000000000000FF) << 56)
         | ((x & 0x000000000000FF00) << 40)
         | ((x & 0x0000000000FF0000) << 24)
         | ((x & 0x00000000FF000000) <<  8)
         | ((x & 0x000000FF00000000) >>  8)
         | ((x & 0x0000FF0000000000) >> 24)
         | ((x & 0x00FF000000000000) >> 40)
         | ((x & 0xFF00000000000000) >> 56);
#else
    return ((x & 0x00000000000000FFULL) << 56)
         | ((x & 0x000000000000FF00ULL) << 40)
         | ((x & 0x0000000000FF0000ULL) << 24)
         | ((x & 0x00000000FF000000ULL) <<  8)
         | ((x & 0x000000FF00000000ULL) >>  8)
         | ((x & 0x0000FF0000000000ULL) >> 24)
         | ((x & 0x00FF000000000000ULL) >> 40)
         | ((x & 0xFF00000000000000ULL) >> 56);
#endif
}

/** Reads 16 bits in network byte order */
VLC_USED
static inline uint16_t U16_AT (const void *p)
{
    uint16_t x;

    memcpy (&x, p, sizeof (x));
    return ntoh16 (x);
}

/** Reads 32 bits in network byte order */
VLC_USED
static inline uint32_t U32_AT (const void *p)
{
    uint32_t x;

    memcpy (&x, p, sizeof (x));
    return ntoh32 (x);
}

/** Reads 64 bits in network byte order */
VLC_USED
static inline uint64_t U64_AT (const void *p)
{
    uint64_t x;

    memcpy (&x, p, sizeof (x));
    return ntoh64 (x);
}

/** Reads 16 bits in little-endian order */
VLC_USED
static inline uint16_t GetWLE (const void *p)
{
    uint16_t x;

    memcpy (&x, p, sizeof (x));
#ifdef WORDS_BIGENDIAN
    x = vlc_bswap16 (x);
#endif
    return x;
}

/** Reads 32 bits in little-endian order */
VLC_USED
static inline uint32_t GetDWLE (const void *p)
{
    uint32_t x;

    memcpy (&x, p, sizeof (x));
#ifdef WORDS_BIGENDIAN
    x = vlc_bswap32 (x);
#endif
    return x;
}

/** Reads 64 bits in little-endian order */
VLC_USED
static inline uint64_t GetQWLE (const void *p)
{
    uint64_t x;

    memcpy (&x, p, sizeof (x));
#ifdef WORDS_BIGENDIAN
    x = vlc_bswap64 (x);
#endif
    return x;
}

#define GetWBE(p)  U16_AT(p)
#define GetDWBE(p) U32_AT(p)
#define GetQWBE(p) U64_AT(p)

#ifndef __MIN
 #define __MIN(a,b) \
   ({ __typeof__ (a) _a = (a); \
       __typeof__ (b) _b = (b); \
     _a < _b ? _a : _b; })
#endif

#define VLC_FOURCC( a, b, c, d ) \
    ( ((uint32_t)a) | ( ((uint32_t)b) << 8 ) \
    | ( ((uint32_t)c) << 16 ) | ( ((uint32_t)d) << 24 ) )

typedef uint32_t vlc_fourcc_t;

/***********************/


#endif /* VLC_COMMON_H_ */
