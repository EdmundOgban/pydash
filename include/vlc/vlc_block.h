#ifndef VLC_BLOCK_H_
#define VLC_BLOCK_H_

/** The content doesn't follow the last block, possible some blocks in between
 *  have been lost */
#define BLOCK_FLAG_DISCONTINUITY 0x0001
/** Intra frame */
#define BLOCK_FLAG_TYPE_I        0x0002
/** Inter frame with backward reference only */
#define BLOCK_FLAG_TYPE_P        0x0004
/** Inter frame with backward and forward reference */
#define BLOCK_FLAG_TYPE_B        0x0008
/** For inter frame when you don't know the real type */
#define BLOCK_FLAG_TYPE_PB       0x0010
/** Warn that this block is a header one */
#define BLOCK_FLAG_HEADER        0x0020
/** This block contains the last part of a sequence  */
#define BLOCK_FLAG_END_OF_SEQUENCE 0x0040
/** This block contains a clock reference */
#define BLOCK_FLAG_CLOCK         0x0080
/** This block is scrambled */
#define BLOCK_FLAG_SCRAMBLED     0x0100
/** This block has to be decoded but not be displayed */
#define BLOCK_FLAG_PREROLL       0x0200
/** This block is corrupted and/or there is data loss  */
#define BLOCK_FLAG_CORRUPTED     0x0400
/** This block is last of its access unit */
#define BLOCK_FLAG_AU_END        0x0800
/** This block contains an interlaced picture with top field stored first */
#define BLOCK_FLAG_TOP_FIELD_FIRST 0x1000
/** This block contains an interlaced picture with bottom field stored first */
#define BLOCK_FLAG_BOTTOM_FIELD_FIRST 0x2000
/** This block contains a single field from interlaced picture. */
#define BLOCK_FLAG_SINGLE_FIELD  0x4000

/** This block contains an interlaced picture */
#define BLOCK_FLAG_INTERLACED_MASK \
    (BLOCK_FLAG_TOP_FIELD_FIRST|BLOCK_FLAG_BOTTOM_FIELD_FIRST|BLOCK_FLAG_SINGLE_FIELD)

#define BLOCK_FLAG_TYPE_MASK \
    (BLOCK_FLAG_TYPE_I|BLOCK_FLAG_TYPE_P|BLOCK_FLAG_TYPE_B|BLOCK_FLAG_TYPE_PB)

/* These are for input core private usage only */
#define BLOCK_FLAG_CORE_PRIVATE_MASK  0x00ff0000
#define BLOCK_FLAG_CORE_PRIVATE_SHIFT 16

/* These are for module private usage only */
#define BLOCK_FLAG_PRIVATE_MASK  0xff000000
#define BLOCK_FLAG_PRIVATE_SHIFT 24

//** The content doesn't follow the last block, possible some blocks in between
 /*  have been lost */
#define BLOCK_FLAG_DISCONTINUITY 0x0001
/** Intra frame */
#define BLOCK_FLAG_TYPE_I        0x0002
/** Inter frame with backward reference only */
#define BLOCK_FLAG_TYPE_P        0x0004
/** Inter frame with backward and forward reference */
#define BLOCK_FLAG_TYPE_B        0x0008
/** For inter frame when you don't know the real type */
#define BLOCK_FLAG_TYPE_PB       0x0010
/** Warn that this block is a header one */
#define BLOCK_FLAG_HEADER        0x0020
/** This block contains the last part of a sequence  */
#define BLOCK_FLAG_END_OF_SEQUENCE 0x0040
/** This block contains a clock reference */
#define BLOCK_FLAG_CLOCK         0x0080
/** This block is scrambled */
#define BLOCK_FLAG_SCRAMBLED     0x0100
/** This block has to be decoded but not be displayed */
#define BLOCK_FLAG_PREROLL       0x0200
/** This block is corrupted and/or there is data loss  */
#define BLOCK_FLAG_CORRUPTED     0x0400
/** This block is last of its access unit */
#define BLOCK_FLAG_AU_END        0x0800
/** This block contains an interlaced picture with top field stored first */
#define BLOCK_FLAG_TOP_FIELD_FIRST 0x1000
/** This block contains an interlaced picture with bottom field stored first */
#define BLOCK_FLAG_BOTTOM_FIELD_FIRST 0x2000
/** This block contains a single field from interlaced picture. */
#define BLOCK_FLAG_SINGLE_FIELD  0x4000

/** This block contains an interlaced picture */
#define BLOCK_FLAG_INTERLACED_MASK \
    (BLOCK_FLAG_TOP_FIELD_FIRST|BLOCK_FLAG_BOTTOM_FIELD_FIRST|BLOCK_FLAG_SINGLE_FIELD)

#define BLOCK_FLAG_TYPE_MASK \
    (BLOCK_FLAG_TYPE_I|BLOCK_FLAG_TYPE_P|BLOCK_FLAG_TYPE_B|BLOCK_FLAG_TYPE_PB)

/* These are for input core private usage only */
#define BLOCK_FLAG_CORE_PRIVATE_MASK  0x00ff0000
#define BLOCK_FLAG_CORE_PRIVATE_SHIFT 16

/* These are for module private usage only */
#define BLOCK_FLAG_PRIVATE_MASK  0xff000000
#define BLOCK_FLAG_PRIVATE_SHIFT 24

#endif /* VLC_BLOCK_H_ */