#include <stdint.h>

#ifndef AAC_HEADER_H_
#define AAC_HEADER_H_

#define AAC_ADTS_MPEG2 1
#define AAC_ADTS_MPEG4 0

typedef union {
    struct {
        uint64_t raw_data_blocks:2;
        uint64_t adts_buffer_fullness:11;
        uint64_t frame_length:13;
        uint64_t copyright_start:1;
        uint64_t copyright_id:1;
        uint64_t home:1;
        uint64_t original:1;
        uint64_t channel_config:3;
        uint64_t private:1;
        uint64_t sample_freq:4;
        uint64_t profile:2;
        uint64_t protect_absent:1;
        uint64_t layer:2;
        uint64_t mpeg_version:1;
        uint64_t sync_code:12;
    } fields;
    uint64_t whole;
} aac_adts_header_t;

uint64_t aac_compile_header(uint32_t, uint8_t, uint16_t);

#endif /* AAC_HEADER_H_ */