#include <stdint.h>

#ifndef RICHBUFFER_H_
#define RICHBUFFER_H_

struct richbuffer {
    void * buf;
    void * scrub;
    size_t size;
    size_t len;
    size_t pos;
};

int richbuff_alloc(struct richbuffer *);
int richbuff_realloc(struct richbuffer *);
void richbuff_wrap(struct richbuffer *, uint8_t *, uint8_t);
void richbuff_free(struct richbuffer *);
inline static void richbuff_reset_scrub(struct richbuffer * buf) {
    buf->scrub = buf->buf;
}

#endif

