#include <stdint.h>

#define PCM_CONVERT_TEMPLATE(dst_buf, src_len, dst_len, sample) \
do {                                                            \
    for (size_t i = 0; i < src_len; sample++, i += 2) {         \
        dst_buf[i] = *sample;                                   \
        dst_buf[i+1] = *sample;                                 \
    }                                                           \
                                                                \
    *dst_len = src_len * 2;                                     \
} while (0)

static inline void pcm_convert_m8_to_s8(struct richbuffer * src,
                                        struct richbuffer * dst) {
    int8_t * sample = (int8_t *)src->buf;
    int8_t * dst_buf = (int8_t *)dst->buf;

    PCM_CONVERT_TEMPLATE(dst_buf, src->len, &dst->len, sample);
}

static inline void pcm_convert_m16_to_s16(struct richbuffer * src,
                                          struct richbuffer * dst) {
    int16_t * sample = (int16_t *)src->buf;
    int16_t * dst_buf = (int16_t *)dst->buf;

    PCM_CONVERT_TEMPLATE(dst_buf, src->len, &dst->len, sample);
}

static inline int pcm_convert_m_to_s(struct richbuffer * pcm_data,
                                     struct richbuffer * pcm_data_a,
                                     struct richbuffer * pcm_data_b,
                                     unsigned int bits_per_sample) {
    struct richbuffer * src;
    struct richbuffer * dst;

    if (pcm_data == pcm_data_a) {
        src = pcm_data_a;
        dst = pcm_data_b;
    }
    else {
        src = pcm_data_b;
        dst = pcm_data_a;
    }

    switch (bits_per_sample) {
        case 8:
            pcm_convert_m8_to_s8(src, dst);
            return 0;
        break;
        case 16:
            pcm_convert_m16_to_s16(src, dst);
            return 0;
        break;
        default:
            return -1;
        break;
    }
}

static inline void pcm_swap_buffers(struct richbuffer ** pcm_data,
                                    struct richbuffer * pcm_data_a,
                                    struct richbuffer * pcm_data_b) {
    *pcm_data = (*pcm_data == pcm_data_a) ? pcm_data_b : pcm_data_a;
}
