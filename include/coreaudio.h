#ifndef COREAUDIO_H_
#define COREAUDIO_H_

struct CoreAudio_layout_s {
    uint32_t i_channels_layout_tag;
    uint32_t i_channels_bitmap;
    uint32_t i_channels_description_count;
    struct {
        uint32_t i_channel_label;
        uint32_t i_channel_flags;
        float    f_coordinates[3];
    } *p_descriptions;
};


static inline void CoreAudio_Layout_Clean(struct CoreAudio_layout_s *c) {
    free( c->p_descriptions );
}

#endif