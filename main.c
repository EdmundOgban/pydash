#include <stdio.h>
#include <stdlib.h>
#include <stdint.h>
#include <stdbool.h>
#include <assert.h>

#include <vlc_common.h>
#include <vlc_stream.h>

#include "libmp4.h"
#include "aac_header.h"
#include "richbuffer.h"
#include "pcm.h"
#include "resample.h"

#define CHUNK_SIZE 8192
#define TARGET_SAMPLERATE 48000

#ifdef HAS_FAAD
    #include <neaacdec.h>
    #define FAAD(code) code
    #define NO_FAAD(code)
#else
    #define FAAD(code)
    #define NO_FAAD(code) code
#endif


FAAD(
void faad_print_frame_info(NeAACDecFrameInfo * frm_info) {
    fprintf(stderr, "decoded %lu samples\n", frm_info->samples);
    fprintf(stderr, "  bytesconsumed: %lu\n", frm_info->bytesconsumed);
    fprintf(stderr, "  channels: %d\n", frm_info->channels);
    fprintf(stderr, "  samplerate: %lu\n", frm_info->samplerate);
    fprintf(stderr, "  sbr: %u\n", frm_info->sbr);
    fprintf(stderr, "  object_type: %u\n", frm_info->object_type);
    fprintf(stderr, "  header_type: %u\n", frm_info->header_type);
    fprintf(stderr, "  num_front_channels: %u\n", frm_info->num_front_channels);
    fprintf(stderr, "  num_side_channels: %u\n", frm_info->num_side_channels);
    fprintf(stderr, "  num_back_channels: %u\n", frm_info->num_back_channels);
    fprintf(stderr, "  num_lfe_channels: %u\n", frm_info->num_lfe_channels);
    fprintf(stderr, "  ps: %u\n", frm_info->ps);
    fprintf(stderr, "\n");
}
);

#define FSTREAM_LEN(len, f)      \
do {                             \
    fseek(f, 0, SEEK_END);       \
    len = ftell(f);              \
    fseek(f, 0, SEEK_SET);       \
} while (0)

int init(uint8_t ** vlc_buf, FILE ** f, FILE ** fw, stream_t ** s) {
    size_t len;

    *f = fopen("sempronio.mp4", "rb");

    if (*f == NULL) {
        fprintf(stderr, "Cannot open input file\n");
        return -1;
    }

    FAAD(
        *fw = fopen("sempronio.wav", "wb");
    );
    NO_FAAD(
        *fw = fopen("sempronio.aac", "wb");
    );

    if (*fw == NULL) {
        fprintf(stderr, "Cannot open output file\n");
        fclose(*f);
        return -1;
    }

    FSTREAM_LEN(len, *f);

    *vlc_buf = vlc_alloc(len, 1);
    if (*vlc_buf == NULL) {
        fprintf(stderr, "Out of memory\n");
        fclose(*f);
        return -2;
    }

    size_t read_bytes = fread(*vlc_buf, 1, len, *f);
    fclose(*f);

    msg_vvDbg(NULL, "read %lu bytes\n", read_bytes);

    *s = vlc_stream_MemoryNew(NULL, *vlc_buf, read_bytes, false);
    if (s == NULL) {
        fprintf(stderr, "Out of memory\n");
        return -3;
    }

    return 0;
}

FAAD(
int faad_decode_next(struct richbuffer * aac_buf, uint8_t ** decoded_buf,
                     NeAACDecFrameInfo *frm_info) {
    static NeAACDecHandle faad_ctx;
    int ret = 0;

    if (faad_ctx == NULL) {
        unsigned long samplerate;
        uint8_t channels;
        int8_t err;

        faad_ctx = NeAACDecOpen();
        err = NeAACDecInit(
            faad_ctx, aac_buf->scrub, aac_buf->len, &samplerate, &channels);
        if (err < 0) {
            fprintf(stderr, "NeAACDecInit error: %d\n", err);
            return -1;
        }
        aac_buf->scrub += err;
        aac_buf->len -= err;
        fprintf(stderr, "{ samplerate: %lu, channels: %u, bytesRead: %d }\n",
            samplerate, channels, err);
    }

    *decoded_buf = NeAACDecDecode(faad_ctx, frm_info, aac_buf->scrub, aac_buf->len);

    if ((frm_info->error == 0) && (frm_info->samples > 0)) {
        // do what you need to do with the decoded samples
        //faad_print_frame_info(frm_info);
        aac_buf->scrub += frm_info->bytesconsumed;
        aac_buf->len -= frm_info->bytesconsumed;
        ret = 1;
    }
    else if (frm_info->error != 0) {
        // Some error occurred while decoding this frame
        fprintf(stderr, "NeAACDecode error: %d\n", frm_info->error);
        fprintf(stderr, "%s\n", NeAACDecGetErrorMessage(frm_info->error));
    }
    else {
      fprintf(stderr, "got nothing...\n");
    }

    return ret;
}
);

int main() {
    uint8_t * vlc_buf;
    FILE * f;
    FILE * fw;
    MP4_Box_t * root_box;
    stream_t * s;
    int ret = 0;


    switch (init(&vlc_buf, &f, &fw, &s)) {
        case -1:
            return -1;
        break;
        case -2:
            ret = -2;
            goto exit_noalloc;
        break;
        case -3:
            ret = -3;
            goto exit_nostream;
        break;
    }

    root_box = MP4_BoxGetRoot(s);

    if (root_box == NULL) {
        msg_Dbg(NULL, "Cannot get root box.");
        ret = -1;
        goto exit;
    }
    
    while (true) {
        struct richbuffer aac_buf;
        struct richbuffer mp4_buf;
        FAAD(
            uint8_t * pcm_buf;
            struct richbuffer pcm_converted;
        );

        MP4_Box_t * trun_box_chunk;
        MP4_Box_t * mdat_box_chunk;
        MP4_Box_t * trun_box_view;
        MP4_Box_t * mdat_box_view;
        MP4_Box_data_trun_t * p_trun;

        trun_box_chunk = MP4_BoxGetNextChunk(s);
        if (trun_box_chunk == NULL)
            break;

        trun_box_view = MP4_BoxGet(trun_box_chunk, "moof/traf/trun");
        if (trun_box_view == NULL)
            continue;

        mdat_box_chunk = MP4_BoxGetNextChunk(s);
        if (mdat_box_chunk == NULL)
            break;

        mdat_box_view = MP4_BoxGet(mdat_box_chunk, "mdat");
        if (mdat_box_view == NULL)
            continue;

        p_trun = trun_box_view->data.p_trun;

        // FIXME: Trusting user data
        mp4_buf.size = mdat_box_view->i_size - 8;
        aac_buf.size = mp4_buf.size + 7 * p_trun->i_sample_count;

        richbuff_alloc(&mp4_buf);
        richbuff_alloc(&aac_buf);

        mp4_buf.len = mp4_buf.size;
        aac_buf.len = aac_buf.size;

        vlc_stream_Seek(s, mdat_box_view->i_pos + 8);
        vlc_stream_Read(s, mp4_buf.buf, mp4_buf.size);

        for (uint32_t i = 0; i < p_trun->i_sample_count; i++) {
            MP4_descriptor_trun_sample_t * sample = &p_trun->p_samples[i];
            uint64_t aac_header = aac_compile_header(44100, 2, sample->i_size);

            // fprintf(stderr, "trun_box sample_count:%u\n", p_trun->i_sample_count);
            // fprintf(stderr, "trun_box p_samples->duration:%u\n", p_trun->p_samples->i_duration);
            // fprintf(stderr, "trun_box p_samples->size:%u\n", p_trun->p_samples->i_size);

            memcpy(aac_buf.scrub, (char *)&aac_header, 7);
            aac_buf.scrub += 7;

            memcpy(aac_buf.scrub, mp4_buf.scrub, sample->i_size);

            aac_buf.scrub += sample->i_size;
            mp4_buf.scrub += sample->i_size;
        }

        FAAD(
            NeAACDecFrameInfo faad_frminfo;

            // memset(&faad_frminfo, 0, sizeof(NeAACDecFrameInfo));
            richbuff_reset_scrub(&aac_buf);

            while (faad_decode_next(&aac_buf, &pcm_buf, &faad_frminfo) > 0) {
                uint8_t channels = faad_frminfo.channels;
                uint64_t samples = faad_frminfo.samples;
                bool converted = false;
                bool resampled = false;

                // printf("Decoded %lu samples\n", samples);

                if (faad_frminfo.samplerate != TARGET_SAMPLERATE) {
                    uint8_t * pcm_resampled_buf;
                
                    samples = samplerate_resample(
                        pcm_buf, samples, &pcm_resampled_buf,
                        faad_frminfo.samplerate, TARGET_SAMPLERATE, channels, 16);
                
                    // printf("Resampled into %lu samples\n", samples);
                    //free(pcm_buf);
                    pcm_buf = pcm_resampled_buf;
                    resampled = true;
                }

                if (channels == 1) {
                    int ret;
                    struct richbuffer pcm_richbuf;

                    if (pcm_converted.buf == NULL) {
                        pcm_converted.size = samples * 2;
                        if (!richbuff_alloc(&pcm_converted)) {
                            fprintf(stderr, "Out of memory");
                            // PY_RAISE_OOM();
                            goto exit;
                        }
                    }

                    richbuff_wrap(&pcm_richbuf, pcm_buf, samples);
                    ret = pcm_convert_m_to_s(&pcm_richbuf, &pcm_richbuf, &pcm_converted, 16);
                    channels = 2;

                    free(pcm_buf);
                    pcm_buf = pcm_converted.buf;
                    converted = true;
                }
                // samples * depth
                fwrite(pcm_buf, samples * 2, 1, fw);

                if (resampled || converted)
                    free(pcm_buf);
            }
        );

        NO_FAAD(
            fwrite(aac_buf.buf, aac_buf.size, 1, fw);
        )

        fprintf(stderr, "    ---- mp4 trun/mdat box done\n");

        richbuff_free(&mp4_buf);
        richbuff_free(&aac_buf);

        MP4_BoxFree(trun_box_chunk);
        MP4_BoxFree(mdat_box_chunk);

    }

exit:
    MP4_BoxFree(root_box);
    vlc_stream_Delete(s);
exit_nostream:
    free(vlc_buf);
exit_noalloc:
    fclose(fw);

    return ret;
}
